
# Rivet for FCC-hh

Contact: Christian Gutschow

## Prerequisites

We recommend to use the latest Rivet Docker image. To download the image, simply type
```
   docker pull hepstore/rivet:3.1.8
```

You can run the container interactively, e.g. like so

```
   docker container run -it -v $PWD:$PWD -w $PWD hepstore/rivet:3.1.8 /bin/bash
```

To check that it worked, try e.g.
```
  rivet --version
```
which should return `rivet v3.1.8`.


Some example HepMC events can be downloaded and untarred as follows

```
   wget "https://rivetval.web.cern.ch/rivetval/TUTORIAL/truth-analysis.tar.gz" -O- | tar -xz --no-same-owner
```

You should see two files `Wjets13TeV_10k.hepmc.gz` and `Zjets13TeV_10k.hepmc.gz`
in your local directory, corresponding to particle-level $`W`$+jets and $`Z`$+jets events in
proton-proton collisons at a centre-of-mass energy of 13 TeV.

## Compiling the routine

You can compile the routine using
```
   rivet-build RivetCUSTOM.so MY_FCC_ANALYSIS.cc
```

## Running the routine

Assuming you compiled the routine as mentiond above and you've downloaded the example HepMC files
mentioned in the Prerequisites, you can run the routine e.g. like so
```
   rivet -a MY_FCC_ANALYSIS --pwd Zjets13TeV_10k.hepmc.gz
```

which should produce an output YODA file called `Rivet.yoda`.
The name of the output file can be specified using the `-o` flag.

## Making plots and uncertainty bands

To plot the outout, simple run
```
rivet-mkhtml --errs --no-weights Rivet.yoda:"Title=Z+jets"
```

In order to pick up the canvas cosmetics from the `.plot` file,
either add `-c MY_FCC_ANALYSIS.plot` to the command or set
```
  export RIVET_ANALYSIS_PATH=`pwd`
```

Note that the example HepMC events come with multiweights
for factorisation and renormalisation scale variations.
An example script (`plot.sh`) is provided that extends the last command,
so as to combine the weights into uncertainty bands on the fly,
differentiating between "stats only", "stats + ME variations"
as well as "stats + ME + PS variations".

