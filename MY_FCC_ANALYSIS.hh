// -*- C++ -*-
#ifndef RIVET_MY_FCC_ANALYSIS_HH
#define RIVET_MY_FCC_ANALYSIS_HH

#include "Rivet/Analysis.hh"
#include "Rivet/Projections/SmearedParticles.hh"
#include "Rivet/Projections/SmearedJets.hh"
#include "Rivet/Projections/SmearedMET.hh"

namespace Rivet{
  namespace FCC_HH {

    /// @name Smearing and efficiency helpers (mostly guesses)
    //@{

    inline double LEP_EFF(const Particle& m) {
      if (m.abseta() > 4.9)  return 0;
      if (m.pT() < 10*GeV)   return 0;
      return (m.abseta() < 2.7) ? 0.95 : 0.85;
    }

    inline Particle LEP_SMEAR(const Particle& m) {
      /// Based on https://arxiv.org/abs/1603.05598 , eq (10) and Fig 12
      double mres_pt = 0.015;
      if (m.pT() > 50*GeV) mres_pt = 0.014 + 0.01*(m.pT()/GeV-50)/50;
      if (m.pT() > 100*GeV) mres_pt = 0.025;
      const double ptres_pt = SQRT2 * mres_pt; //< from Eq (10)
      const double resolution = (m.abseta() < 1.5 ? 1.0 : 1.25) * ptres_pt;
      return Particle(m.pid(), P4_SMEAR_PT_GAUSS(m, resolution*m.pT()));
    }

    inline double PHOTON_EFF(const Particle& y) {
      // Based on ATL-PHYS-PUB-2016-014 , Fig 6
      if (y.abspid() != PID::PHOTON)  return 0; ///< Could also allow for misID
      if (y.pT() < 10*GeV)  return 0;
      if (inRange(y.abseta(), 1.37, 1.52) || y.abseta() > 6.0) return 0;
      static const vector<double> edges_eta = { 0., 0.6, 1.37, 1.52, 1.81, 6.0 }; //< 2.37 adjusted to 6.0
      static const vector<double> edges_pt = { 10., 15., 20., 25., 30., 35., 40., 45.,
                                               50., 60., 80., 100., 125., 150., 175., 250. };
      static const vector<double> effs = {0.55, 0.70, 0.85, 0.89, 0.93, 0.95, 0.96, 0.96,
                                          0.97, 0.97, 0.98, 0.97, 0.97, 0.97, 0.97, 0.97,// next eta bin
                                          0.47, 0.66, 0.79, 0.86, 0.89, 0.94, 0.96, 0.97,
                                          0.97, 0.98, 0.97, 0.98, 0.98, 0.98, 0.98, 0.98,// next eta bin
                                          0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,
                                          0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,// next eta bin
                                          0.54, 0.71, 0.84, 0.88, 0.92, 0.93, 0.94, 0.95,
                                          0.96, 0.96, 0.96, 0.96, 0.96, 0.96, 0.96, 0.96,// next eta bin
                                          0.61, 0.74, 0.83, 0.88, 0.91, 0.94, 0.95, 0.96,
                                          0.97, 0.98, 0.98, 0.98, 0.98, 0.98, 0.98, 0.98};
      const int i_eta = binIndex(y.abseta(), edges_eta);
      const int i_pt = binIndex(y.pT()/GeV, edges_pt, true);
      const int i = i_eta*edges_pt.size() + i_pt;
      const double eff = effs[i];
      return eff;
    }

    inline Jet JET_SMEAR(const Jet& j) {
      // Jet energy resolution lookup based on
      // https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2015-017/
      static const vector<double> binedges_pt = {0., 50., 70., 100., 150., 200., 1000., 10000.};
      static const vector<double> jer = {0.145, 0.115, 0.095, 0.075, 0.07, 0.05, 0.04, 0.04}; //< note overflow value
      const int ipt = binIndex(j.pT()/GeV, binedges_pt, true);
      if (ipt < 0) return j;
      const double resolution = jer.at(ipt);

      // Smear by a Gaussian centered on 1 with width given by the (fractional) resolution
      const double fsmear = max(randnorm(1., resolution), 0.);
      const double mass = j.mass2() > 0 ? j.mass() : 0; //< numerical carefulness...
      Jet rtn(FourMomentum::mkXYZM(j.px()*fsmear, j.py()*fsmear, j.pz()*fsmear, mass));
      return rtn;
    }

    inline Vector3 MET_SMEAR(const Vector3& met, double set) {
      // Based on https://arxiv.org/pdf/1802.08168.pdf, Figs 6-9
      Vector3 smeared_met = met;

      // Linearity offset (Fig 6)
      if (met.mod() < 25*GeV)  smeared_met *= 1.5;
      else smeared_met *= (1 + exp(-(met.mod() - 25*GeV)/(10*GeV)) - 0.02); //< exp approx to Fig 6 curve, approaching -0.02

      // Resolution(sumEt) ~ 0.45 sqrt(sumEt) GeV
      // above SET = 180 GeV, and with linear trend from SET = 180 -> 0  to resolution = 0 (Fig 7)
      const double resolution1 = (set < 180*GeV ? set/180. : 1) * 0.45 * sqrt(max(set/GeV, 180)) * GeV;
      /// @todo Allow smearing function to access the whole event, since Njet also affects? Or assume encoded in SET?

      // Resolution(MET_true) (Fig 9)
      const double resolution2 = 15*GeV + 0.5*sqrt(met.mod()/GeV)*GeV;

      // Smear by a Gaussian with width given by the minimum resolution estimator
      // (should mean low-MET events with high SET do not get a large smearing,
      // and will be dominated by the linearity effect).
      const double resolution = min(resolution1, resolution2);
      const double metsmear = fabs(randnorm(smeared_met.mod(), resolution));
      smeared_met = metsmear * smeared_met.unit();

      return smeared_met;
    }

    //@}

  }
}

#endif
