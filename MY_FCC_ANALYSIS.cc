// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

#include "MY_FCC_ANALYSIS.hh"

namespace Rivet {

  /// @brief Example routine for an FCC-hh setup
  class MY_FCC_ANALYSIS : public Analysis {
  public:

    /// Constructor
    RIVET_DEFAULT_ANALYSIS_CTOR(MY_FCC_ANALYSIS);

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Optional options
      _lmode = 0; // default accepts either channel
      if ( getOption("LMODE") == "EL" )  _lmode = 1;
      if ( getOption("LMODE") == "MU" )  _lmode = 2;

      _doTruth = getOption<bool>("TRUTH", 0);

      // Smear leptons
      const FinalState leps(Cuts::abseta < 6.0 && Cuts::pT > 10*GeV &&
                           (Cuts::abspid == PID::MUON || Cuts::abspid == PID::ELECTRON));
      if (_doTruth)  declare(leps, "Leptons");
      else {
        declare(SmearedParticles(leps, FCC_HH::LEP_EFF, FCC_HH::LEP_SMEAR), "Leptons");
      }

      // Smear photons
      const FinalState photons(Cuts::abspid == PID::PHOTON && Cuts::abseta < 6.0 && Cuts::pT > 10*GeV);
      if (_doTruth)  declare(photons, "Photons");
      else {
        declare(SmearedParticles(photons, FCC_HH::PHOTON_EFF), "Photons");
      }

      // Smear jet collection
      const FinalState calo_fs(Cuts::abseta < 6.0);
      const FastJets jets(calo_fs, FastJets::ANTIKT, 0.4);
      if (_doTruth) declare(jets, "Jets");
      else {
        declare(SmearedJets(jets,  FCC_HH::JET_SMEAR, // JER binned in pT
                // JET_BTAG_EFFS(0.7, 0.1, 0.01) // <- fixed efficiencies/rejection rates ?
                [](const Jet& j) { // or use lambda for b-tagging efficiencies
                if (j.abseta() > 5.8)  return 0.;
                return j.bTagged(Cuts::pT > 5*GeV) ? 0.77 :
                j.cTagged(Cuts::pT > 5*GeV) ? 1/10. : 1/100.; }), "Jets");
      }

      // Smear MET
      if (_doTruth)  declare(MissingMomentum(calo_fs), "MET");
      else           declare(SmearedMET(MissingMomentum(calo_fs), FCC_HH::MET_SMEAR), "MET");


      // Book histograms
      vector<double> mll_bins = { 66., 74., 78., 82., 84., 86., 88., 89., 90., 91.,
                                  92., 93., 94., 96., 98., 100., 104., 108., 116. };
      book(_h["mll"],        "mass_ll",         mll_bins);
      book(_h["jets_excl"],  "jets_excl",  6, -0.5,  5.5);
      book(_h["bjets_excl"], "bjets_excl", 3, -0.5,  2.5);
      book(_h["HT"],         "HT",         6,  20., 110.);
    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {

      Particles leptons = apply<ParticleFinder>(event, "Leptons").particles();

      if (leptons.size() != 2)  vetoEvent; // require exactly two leptons
      //if (leptons[0].abspid() != leptons[1].abspid())     vetoEvent; // same flavour
      if (leptons[0].charge() * leptons[1].charge() > 0)  vetoEvent; // opposite charge

      // Support running with a single channel, if the LMODE option is set
      if (_lmode == 1 && leptons[0].abspid() != PID::ELECTRON)  vetoEvent;
      else if (_lmode == 2 && leptons[0].abspid() != PID::MUON) vetoEvent;

      const double mll = (leptons[0].mom() + leptons[1].mom()).mass()/GeV;
      _h["mll"]->fill(mll);

      if (!inRange(mll, 66*GeV, 116*GeV))  vetoEvent;

      Jets jets = apply<JetAlg>(event, "Jets").jetsByPt(Cuts::pT > 20*GeV && Cuts::abseta < 5.8);
      idiscardIfAnyDeltaRLess(jets,  leptons, 0.2);

      _h["jets_excl"]->fill(jets.size());

      const double HT = sum(jets, Kin::pT, 0.0)/GeV;
      _h["HT"]->fill(HT);

      size_t bTags = count(jets, hasBTag(Cuts::pT > 5*GeV));
      // Or like so:
      /*Jets bjets;
      for (const Jet& j : jets) {
        if (j.bTagged(Cuts::pT > 5*GeV))  bjets += j;
      }*/
      _h["bjets_excl"]->fill(bTags);
    }

    /// Normalise histograms etc., after the run
    void finalize() {

      const double sf = crossSection() / sumOfWeights();
      scale(_h, sf);

    }

    //@}

    /// @name Histograms
    //@{

    map<string, Histo1DPtr> _h;
    size_t _lmode, _doTruth;

    //@}

  };

  // The hook for the plugin system
  RIVET_DECLARE_PLUGIN(MY_FCC_ANALYSIS);
}
